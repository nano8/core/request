### nano/core/request

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-request?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-request)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-request?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-request)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/request/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/request/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/request/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/request/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/request/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/request/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-request`
