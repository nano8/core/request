<?php

namespace laylatichy\nano\core;

use Workerman\Protocols\Http\Request as WorkermanRequest;

final class Request {
    public const OPTIONS = 'OPTIONS';

    public function __construct(
        private WorkermanRequest $request,
    ) {
    }

    public function post(string $name = null): mixed {
        return $name ? $this->request->post($name) : $this->request->post();
    }

    public function get(string $name = null): mixed {
        return $name ? $this->request->get($name) : $this->request->get();
    }

    public function file(string $name = null): mixed {
        return $name ? $this->request->file($name) : $this->request->file();
    }

    public function header(string $name = null): mixed {
        return $name ? $this->request->header($name) : $this->request->header();
    }

    public function cookie(string $name = null): mixed {
        return $name ? $this->request->cookie($name) : $this->request->cookie();
    }

    public function path(): mixed {
        return $this->request->path();
    }
}

